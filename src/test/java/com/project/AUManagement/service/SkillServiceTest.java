package com.project.AUManagement.service;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

import com.project.AUManagement.domain.Skill;
import com.project.AUManagement.repository.GradRepository;
import com.project.AUManagement.repository.SkillRepository;

@SpringBootTest
@TestComponent
//@RunWith(MockitoJUnitRunner.class)
@ExtendWith(SpringExtension.class)
@RunWith( SpringRunner.class )
public class SkillServiceTest {
	@Autowired
	private SkillRepository skillRepository;
	@Autowired
	private SkillService skillService;
	@Test
	public void save()
	{
			Skill skill=new Skill("testskill");
			Skill savedSkill=skillService.save(skill);
			assertEquals(skill.getName(),savedSkill.getName());
			List<Skill> skillList=skillService.findAll();
			assertEquals(skillList.get(skillList.size()-1).getName(),"testskill");
			skillRepository.deleteTestSkill();
			
	}

}
