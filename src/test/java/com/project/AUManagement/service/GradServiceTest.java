package com.project.AUManagement.service;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

import com.project.AUManagement.domain.Grad;
import com.project.AUManagement.domain.Skill;
import com.project.AUManagement.domain.Trend;
import com.project.AUManagement.repository.GradRepository;
import com.project.AUManagement.repository.SkillRepository;

@SpringBootTest
@TestComponent
//@RunWith(MockitoJUnitRunner.class)
@ExtendWith(SpringExtension.class)
@RunWith( SpringRunner.class )
public class GradServiceTest {
	
	@Autowired
	private GradRepository gradRepository;
	@Autowired
	private GradService gradService;
	@Autowired
	private SkillRepository skillRepository;
	
	@Test
	public void findAll() {
		
        Grad grad=new Grad("harman");
        grad.setSkills(new ArrayList<Skill>());
        grad.setJoiningDate(LocalDate.now().toString());
        Grad savedGrad=gradRepository.save(grad);
        
        //System.out.println(savedGrad.getId());
        List<Grad> gradList=gradService.findAll();
        
        assertEquals(gradList.get(gradList.size()-1).getFirstName(), grad.getFirstName());
        gradRepository.deleteForTesting(savedGrad);
	}
	
	@Test
	public void UpdateById()
	{
        Grad grad=new Grad("harman");
        grad.setSkills(new ArrayList<Skill>());
        grad.setJoiningDate(LocalDate.now().toString());
        Grad updatedGrad=gradService.updateById(grad);
        //System.out.println(grad.getId());
        assertEquals(grad.getFirstName(), grad.getFirstName());
        gradRepository.deleteForTesting(updatedGrad);
	}
	@Test
	public void findTrend()
	{
        Grad grad=new Grad("harman");
        grad.setSkills(new ArrayList<Skill>());
        grad.setJoiningDate(LocalDate.now().toString());
        grad.setJoiningLocation("notalocation");
        Grad savedGrad=gradRepository.save(grad);
        List<Trend> trendList=gradService.findTrend("2020","location");
        String name=trendList.get(trendList.size()-1).getName();
        String count=trendList.get(trendList.size()-1).getCount();
        assertEquals(name, "notalocation");
        //System.out.println(count);
        assertEquals(count,"1");
        //System.out.println(savedGrad.getId());
//		Skill skill=new Skill("testskill");
//		Skill savedSkill=skillRepository.save(skill);
//		
//        name=trendList.get(trendList.size()-1).getName();
//        count=trendList.get(trendList.size()-1).getCount();
//        trendList=gradService.findTrend("2020","skill");
//        skillRepository.deleteTestSkill();
        gradRepository.deleteForTesting(savedGrad);
	}

	
//	@Test
//	public void exceptionTest()
//	{
//		throw new ArithmeticException();
//	}
}
