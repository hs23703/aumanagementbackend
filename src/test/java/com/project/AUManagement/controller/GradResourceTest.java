package com.project.AUManagement.controller;

import java.util.List;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.http.HttpStatus;


import com.project.AUManagement.service.GradService;
import com.project.AUManagement.service.Validator;
import com.project.AUManagement.domain.Grad;
import com.project.AUManagement.domain.Trend;
import com.project.AUManagement.domain.Error;


@SpringBootTest
@TestComponent
@RunWith(MockitoJUnitRunner.class)
public class GradResourceTest {
	   @Mock
	    GradService gradService;
	    @Mock
	    Validator validator;
	    @InjectMocks
	    GradResource gradResource;

	    @Test
	   public void getAllGrads() throws GeneralSecurityException, IOException {
	        Mockito.when(validator.isValid("123456")).thenReturn(true);
	        List<Grad> gradList = new ArrayList<Grad>(); 
	        for(int i=0;i<10;i++){
	            Grad testGrad = new Grad();
	            testGrad.setEmail("grad"+i+"@gmail.com");
	            gradList.add(testGrad);
	        }
	        
	        Mockito.when(gradService.findAll()).thenReturn(gradList);
	        List<Grad> responsegradList = (List<Grad>) gradResource.getAllGrads("123456").getBody();
	        assertEquals(10,responsegradList.size());
	        assertEquals(gradList.get(0).getEmail(),responsegradList.get(0).getEmail());
	        Error ResponseError = (Error)gradResource.getAllGrads("sampleToken").getBody();
	        assertEquals("error",ResponseError.message);
	        assertEquals(HttpStatus.FORBIDDEN,gradResource.getAllGrads("sampleToken").getStatusCode());
	    }
	    
	    @Test
	   public void getGrad() throws GeneralSecurityException, IOException
	    {
	     Mockito.when(validator.isValid("goodtoken")).thenReturn(true);
	     Grad grad=new Grad("harman");
	     
	     Mockito.when(gradService.findById(12)).thenReturn(grad);
	     Grad responseGrad= (Grad) gradResource.getGrad(12,"goodtoken").getBody();
	     assertEquals("harman",responseGrad.getFirstName());
	     Error error=(Error) gradResource.getGrad(12,"badtoken").getBody();
	     assertEquals("error",error.message);
	    }
	    
	    @Test
	    public void addGrad() throws GeneralSecurityException, IOException
	    {
		     Mockito.when(validator.isValid("goodtoken")).thenReturn(true);
		     Grad grad=new Grad("harman");
		     Mockito.when(gradService.save(grad)).thenReturn(grad);
		     Grad responseGrad= (Grad) gradResource.addGrad(grad,"goodtoken").getBody();
		     assertEquals("harman",responseGrad.getFirstName());
		     Error responseError=(Error) gradResource.addGrad(grad,"badtoken").getBody();
		     assertEquals("error",responseError.message);
	    }
	    
	    @Test
	    public void updateGrad() throws GeneralSecurityException, IOException
	    {
		     Mockito.when(validator.isValid("goodtoken")).thenReturn(true);
		     Grad grad=new Grad("harman");
		     Mockito.when(gradService.updateById(grad)).thenReturn(grad);
		     Grad responseGrad= (Grad) gradResource.updateGrad(12,grad,"goodtoken").getBody();
	         assertEquals("harman",responseGrad.getFirstName());
			 Error responseError=(Error) gradResource.updateGrad(12,grad,"badtoken").getBody();
			 assertEquals("error",responseError.message);
	    	
	    }
	    @Test
	    public void deleteGrad() throws GeneralSecurityException, IOException
	    {
		     Mockito.when(validator.isValid("goodtoken")).thenReturn(true);
		     Grad grad=new Grad("harman");
		     Mockito.when(gradService.deleteById(12)).thenReturn(grad);
		     Grad responseGrad= (Grad) gradResource.deleteGrad(12,"goodtoken").getBody();
	         assertEquals("harman",responseGrad.getFirstName());
			 Error responseError=(Error) gradResource.deleteGrad(12,"badtoken").getBody();
			 assertEquals("error",responseError.message);
	    }
	    @Test
	    public void getTrend() throws GeneralSecurityException, IOException
	    {
	    	List<Trend> trendList=new ArrayList<Trend>();
	    	for(int i=0;i<5;i++)
	    	{
	    		Trend trend=new Trend();
	    		trendList.add(trend);
	    	}
		     Mockito.when(validator.isValid("goodtoken")).thenReturn(true);
		     Mockito.when(gradService.findTrend("2020","location")).thenReturn(trendList);
	        List<Trend> responseList=(List<Trend>) gradResource.getTrend("2020","location","goodtoken").getBody();
            assertEquals(trendList.size(),responseList.size());
       	    Error responseError=(Error) gradResource.getTrend("2020","location","badtoken").getBody();
		    assertEquals("error",responseError.message);
	     
	    }

}

