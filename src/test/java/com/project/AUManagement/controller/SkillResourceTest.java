package com.project.AUManagement.controller;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import com.project.AUManagement.domain.Error;
import com.project.AUManagement.domain.Skill;
import com.project.AUManagement.repository.SkillRepository;
import com.project.AUManagement.service.SkillService;
import com.project.AUManagement.service.Validator;

@SpringBootTest
@TestComponent
@RunWith(MockitoJUnitRunner.class)
public class SkillResourceTest {
	
	@Mock
	private SkillService skillService;
	@Mock
	private Validator validator;
	@InjectMocks
	private SkillResource skillResource;

	@Test
	public void getAll( ) throws GeneralSecurityException, IOException
	{
		List<Skill> skillList= new ArrayList<Skill>();
		for(int i=0;i<5;i++)
		{
			Skill skill=new Skill();
			skillList.add(skill);
		}
		
		Mockito.when(validator.isValid("goodtoken")).thenReturn(true);
		Mockito.when(skillService.findAll()).thenReturn(skillList);
	    List<Skill> responseList=(List<Skill>) skillResource.getAll("goodtoken").getBody();
	    assertEquals(responseList.size(),skillList.size());
	     Error responseError=(Error) skillResource.getAll("badtoken").getBody();
	     assertEquals("error",responseError.message);
	}
	@Test
	public void getskill( ) throws GeneralSecurityException, IOException 
	{
		Skill skill=new Skill();
		skill.setName("java");
		Mockito.when(validator.isValid("goodtoken")).thenReturn(true);
		Mockito.when(skillService.findById(23)).thenReturn(skill);
		Skill responseSkill=(Skill) skillResource.getskill(23,"goodtoken").getBody();
	    assertEquals(responseSkill.getName(),skill.getName());
	     Error responseError=(Error) skillResource.getskill(23,"badtoken").getBody();
	     assertEquals("error",responseError.message);
	}
	@Test
	public void addSkill( ) throws GeneralSecurityException, IOException
	{
		Skill skill=new Skill();
		skill.setName("java");
		Mockito.when(validator.isValid("goodtoken")).thenReturn(true);
		Mockito.when(skillService.save(skill)).thenReturn(skill);
		Skill responseSkill=(Skill) skillResource.addSkill(skill,"goodtoken").getBody();
	    assertEquals(responseSkill.getName(),skill.getName());
	     Error responseError=(Error) skillResource.addSkill(skill,"badtoken").getBody();
	     assertEquals("error",responseError.message);
	}
}
