package com.project.AUManagement.controller;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.security.GeneralSecurityException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestComponent;
import com.project.AUManagement.domain.User;
import com.project.AUManagement.repository.UserRepository;
import com.project.AUManagement.service.Validator;

@SpringBootTest
@TestComponent
@RunWith(MockitoJUnitRunner.class)
public class LoginResourceTest {

	@Mock
	private Validator validator;
	@Mock
	private UserRepository userRepository;
	@InjectMocks
	private LoginResource loginResource;
	@Test
	public void validateUser() throws GeneralSecurityException, IOException
	{
		
	  User loginUser=new User();
	  loginUser.setIdToken("harman");
	  Mockito.when(validator.isValid(loginUser.getIdToken())).thenReturn(true);
	 // Mockito.when(userRepository.save(loginUser)).thenReturn();
	  boolean response=loginResource.validateUser(loginUser);
	  assertEquals(response,true);


	}
}
