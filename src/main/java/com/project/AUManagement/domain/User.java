package com.project.AUManagement.domain;

public class User {
	private String authToken;
	private String email;
	private String firstName;
	private String id;
	private String idToken;
	private String lastName;
	private String name;
	private String photoUrl;
	private String provider;
	public String getAuthToken() {
		return authToken;
	}
	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdToken() {
		return idToken;
	}
	public void setIdToken(String idToken) {
		this.idToken = idToken;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhotoUrl() {
		return photoUrl;
	}
	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}
	public String getProvider() {
		return provider;
	}
	public void setProvider(String provider) {
		this.provider = provider;
	}
	
	
}
//authToken: "ya29.a0AfH6SMC1arWu_qWQK5EirrJAA0j60Z3WA07hK0tlkQskIQMLuFrsv3POZB5BeqdZqkf3Dfnfx6KX7BjXPoKW6QM7kCEloDdgj4lStGp1QJFYURolRmFu_VcV8oEKrdCFx6TywaulGE5v1De3X3vKMGmRzCCl7pyRE-I"
//email: "hs23703@gmail.com"
//firstName: "Gurharmanjit"
//id: "117368207442759807535"
//idToken: "eyJhbGciOiJSUzI1NiIsImtpZCI6ImZiOGNhNWI3ZDhkOWE1YzZjNjc4ODA3MWU4NjZjNmM0MGYzZmMxZjkiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJhY2NvdW50cy5nb29nbGUuY29tIiwiYXpwIjoiNjc1Mzk1MTU5NDk4LTRvc2NqaTY1dmM3N3NzYXB1cDFxZmQ2ODVhYnFkbjRwLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwiYXVkIjoiNjc1Mzk1MTU5NDk4LTRvc2NqaTY1dmM3N3NzYXB1cDFxZmQ2ODVhYnFkbjRwLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwic3ViIjoiMTE3MzY4MjA3NDQyNzU5ODA3NTM1IiwiZW1haWwiOiJoczIzNzAzQGdtYWlsLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJhdF9oYXNoIjoiRHVldS1KTjczU0xqeDIxNjVvNTRXUSIsIm5hbWUiOiJHdXJoYXJtYW5qaXQgU2luZ2giLCJwaWN0dXJlIjoiaHR0cHM6Ly9saDYuZ29vZ2xldXNlcmNvbnRlbnQuY29tLy1mSm54M2JlM1JEZy9BQUFBQUFBQUFBSS9BQUFBQUFBQUFBQS9BTVp1dWNuTi1kZG1vTWdCRXdXOVJNM01QZkxwTy1FdWpRL3M5Ni1jL3Bob3RvLmpwZyIsImdpdmVuX25hbWUiOiJHdXJoYXJtYW5qaXQiLCJmYW1pbHlfbmFtZSI6IlNpbmdoIiwibG9jYWxlIjoiZW4tR0IiLCJpYXQiOjE1OTEwMzQ2MjMsImV4cCI6MTU5MTAzODIyMywianRpIjoiMWNkY2EyMmZmMTY2NjE1ZTJmZTUwOWUxZTdlOGNmNDM1YzE1ZTdmNiJ9.dr-3u0sigplK8uSjlcJlKOXDupaUvtDm1uOF7-cwLLZSetn4dCjLvUeyfH8UDcux9miA9PFqY41GIwgiSRDlXr08ETCJdyYnWO97xNADTuwPm-nMxj80pdR1p1qutaaVjQq-JLrhEqqniWM3gpsawvmtYsbwX71UfW6CS2I402GzqoexsRpQaPWWh16dpBxT4mhE-DRLZl95XGrDLwjEf81vHlVXwt6tvcvifqNFTNmB2LPTWCu7DBJMnTmjfGZd13xkb7H1Pk6Dd3zibZki-m6KdWW-5PsgtKwyZi2TUrHE22UdbZFLcf4J2TSfvCuoAOVqeIhYkuFtvdThkyJXiw"
//lastName: "Singh"
//name: "Gurharmanjit Singh"
//photoUrl: "https://lh6.googleusercontent.com/-fJnx3be3RDg/AAAAAAAAAAI/AAAAAAAAAAA/AMZuucnN-ddmoMgBEwW9RM3MPfLpO-EujQ/s96-c/photo.jpg"
//provider: "GOOGLE"
