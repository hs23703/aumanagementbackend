package com.project.AUManagement.domain;


public class Skill {

	
     private long id;
	 public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	private String name;
     private String	createdBy;
	 private String	createdOn;
	 
	 public Skill()
	 {}
	 
	public Skill(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}
	 
	 

}
