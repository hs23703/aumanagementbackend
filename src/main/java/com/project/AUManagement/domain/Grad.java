package com.project.AUManagement.domain;


import java.util.ArrayList;
import java.util.List;



//import javax.persistence.JoinTable;
//import javax.persistence.ManyToMany;
//
//import org.hibernate.mapping.Set;

//@Entity
public class Grad {
	
	//@Id
	//@GeneratedValue(strategy= GenerationType.AUTO)
	private long id;
	private long parentId;
	private String firstName;
	private String middleName;
	private String lastName;
	private String createdBy;
	private String createdOn;
	private String gender;
	private String email;
	private String contact;
	private String institute;
	private String degree;
	private String branch;
	private String joiningDate;
	private String joiningLocation;
//	@ManyToMany
//	@JoinTable( name="grad_skill",
//	 joinColumns=@JoinColumn(name="grad_id"),
//	 inverseJoinColumns=@JoinColumn(name="skill_id"))
	private List<Skill> skills= new ArrayList<Skill> ();
	

	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public void setParentId(long parentId) {
		this.parentId = parentId;
	}
	
	public long getParentId() {
		return parentId;}

	public Grad() {
		
	}

	
	public Grad(String firstName) {
		this.firstName = firstName;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getMiddleName() {
		return middleName;
	}


	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getCreatedBy() {
		return createdBy;
	}


	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}



	public String getCreatedOn() {
		return createdOn;
	}


	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}


	public String getGender() {
		return gender;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getContact() {
		return contact;
	}


	public void setContact(String contact) {
		this.contact = contact;
	}


	public String getInstitute() {
		return institute;
	}


	public void setInstitute(String institute) {
		this.institute = institute;
	}


	public String getDegree() {
		return degree;
	}


	public void setDegree(String degree) {
		this.degree = degree;
	}


	public String getBranch() {
		return branch;
	}


	public void setBranch(String branch) {
		this.branch = branch;
	}


	public String getJoiningDate() {
		return joiningDate;
	}


	public void setJoiningDate(String joiningDate) {
		this.joiningDate = joiningDate;
	}


	public String getJoiningLocation() {
		return joiningLocation;
	}


	public void setJoiningLocation(String joiningLocation) {
		this.joiningLocation = joiningLocation;
	}


	public List<Skill> getSkills() {
		return skills;
	}


	public void setSkills(List<Skill> skills) {
		this.skills = skills;
	}

	

}
