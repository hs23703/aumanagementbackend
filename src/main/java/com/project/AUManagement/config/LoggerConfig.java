package com.project.AUManagement.config;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.project.AUManagement.AuManagementApplication;

public class LoggerConfig {
    public static final Logger logger = LogManager.getLogger(AuManagementApplication.class.getName());
}
