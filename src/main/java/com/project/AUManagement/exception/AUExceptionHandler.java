package com.project.AUManagement.exception;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.SQLException;
import org.apache.logging.log4j.core.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import com.project.AUManagement.config.LoggerConfig;

@ControllerAdvice
public class AUExceptionHandler {

		private final Logger logger = (Logger) LoggerConfig.logger;

		private static final String NESTED_EXCEPTION_STRING_CONSTANT = " nested exception is";

		@ExceptionHandler(RuntimeException.class)
		public ResponseEntity handleException(RuntimeException re) {
			try {
				if (re instanceof NullPointerException) {
					NullPointerException npe = (NullPointerException) re;
					String npeString = npe.toString();
					logger.error(npeString);
					Error error = new Error("internal server problem");
					logger.info("nullpointer exception - trying to access the null object");
					return new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
				}
		

				if (re instanceof ArithmeticException) {
					ArithmeticException ae = (ArithmeticException) re;
					logger.error(ae.toString());
					Error error = new Error("Resource Not Found");
					logger.info("Encountered WanderResourceNotFoundException - returning NOT_FOUND");
					return new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
				}

				if (re instanceof ArrayIndexOutOfBoundsException) {
					ArrayIndexOutOfBoundsException ae = (ArrayIndexOutOfBoundsException) re;
					logger.error(ae.toString());
					Error error = new Error("ArrayIndexOutOfBoundsException");
					logger.info("Encountered WanderPermissionDeniedException - returning FORBIDDEN");
					return new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
				}
				if (re instanceof IllegalArgumentException) {
					IllegalArgumentException ie = (IllegalArgumentException) re;
					logger.error(ie.toString());
					Error error = new Error("IllegalArgumentException");
					logger.info("Encountered IllegalArgumentException - returning FORBIDDEN");
					return new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
				}
				
				return resourceExceptionHandler(re);
			} 
			
				catch (Exception e) {
				logger.error("Encountered exception while processing runtime exception", e);
				return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
		// Over ride me - and explicitly add any exceptions you need or want from your service.
		// The goal being that each service will know better which exceptions match to which errors
		// than some general library.
		public ResponseEntity resourceExceptionHandler(RuntimeException re) {
			logger.error("Unexpected RuntimeException encountered", re);
			return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}

}
