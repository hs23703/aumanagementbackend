package com.project.AUManagement.controller;

import java.io.IOException;
import java.security.GeneralSecurityException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.project.AUManagement.domain.User;
import com.project.AUManagement.repository.UserRepository;
import com.project.AUManagement.service.Validator;


@CrossOrigin(origins="*",allowedHeaders = "*")
@RestController
public class LoginResource {
	
@Autowired
private Validator validator;
@Autowired
private UserRepository userRepository;

@PostMapping("/login")
public boolean validateUser(@RequestBody User loginUser) throws GeneralSecurityException, IOException
{
	
	if(validator.isValid(loginUser.getIdToken()))
			{
		       userRepository.save(loginUser);
		        return true;
			}
	return false;
	
}
}
