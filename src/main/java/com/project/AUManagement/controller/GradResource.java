package com.project.AUManagement.controller;


import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.project.AUManagement.service.GradService;
import com.project.AUManagement.service.Validator;
import com.project.AUManagement.domain.Grad;
import com.project.AUManagement.domain.Trend;
import com.project.AUManagement.config.LoggerConfig;
import com.project.AUManagement.domain.Error;

@CrossOrigin(origins="*",allowedHeaders = "*")
@RestController
public class GradResource {
 
	@Autowired
	private GradService gradService;
	
	@Autowired
	private Validator validator;
	
	
	
	
@GetMapping("/grads")
public ResponseEntity<?> getAllGrads(@RequestHeader("token") String token ) throws GeneralSecurityException, IOException
{
//	System.out.println(validator.isValid(token));
	if(validator.isValid(token))
		{
		LoggerConfig.logger.info("valid token");
		List<Grad> arl;
		 arl=gradService.findAll();
		 return new ResponseEntity<>(arl, HttpStatus.OK);
		}
	LoggerConfig.logger.info("invalid Token");
    Error error = new Error();
    error.message = "error";
	return new ResponseEntity<>(error, HttpStatus.FORBIDDEN);
	
}
	
@GetMapping("/grads/{id}")
public ResponseEntity<?> getGrad (@PathVariable long id, @RequestHeader("token") String token ) throws GeneralSecurityException, IOException
{
		if(validator.isValid(token))
		{ 
			LoggerConfig.logger.info("valid token");
			 Grad grad=gradService.findById(id);
			 return new ResponseEntity<>(grad, HttpStatus.OK);
		}
		LoggerConfig.logger.info("invalid token");
    Error error = new Error();
    error.message = "error";
	return new ResponseEntity<>(error, HttpStatus.ACCEPTED);
}    
@PostMapping("/grads")
public ResponseEntity<?> addGrad(@RequestBody Grad g1 ,@RequestHeader("token") String token ) throws GeneralSecurityException, IOException
{
	if(validator.isValid(token))
	{ 
		LoggerConfig.logger.info("valid token");
		Grad grad=gradService.save(g1);
		return new ResponseEntity<>(grad, HttpStatus.CREATED);
	}
	
	LoggerConfig.logger.info("invalid token");
 Error error = new Error();
 error.message = "error";
 return new ResponseEntity<>(error, HttpStatus.ACCEPTED);
}

@PutMapping("/grads/{id}")
public ResponseEntity<?> updateGrad(@PathVariable long id,@RequestBody Grad g1 ,@RequestHeader("token") String token ) throws GeneralSecurityException, IOException
{
	//	if(validtor(header.token))
	if(validator.isValid(token))
	{ LoggerConfig.logger.info("valid token");
		Grad grad=gradService.updateById(g1);
		return new ResponseEntity<>(grad, HttpStatus.ACCEPTED);
	}
	LoggerConfig.logger.info("invalid token");
 Error error = new Error();
 error.message = "error";
 return new ResponseEntity<>(error, HttpStatus.ACCEPTED);
	
}

@DeleteMapping("/grads/{id}")
public ResponseEntity<?> deleteGrad(@PathVariable long id ,@RequestHeader("token") String token ) throws GeneralSecurityException, IOException
{
	if(validator.isValid(token))
	{ 
		LoggerConfig.logger.info("valid token");
	    Grad grad=gradService.deleteById(id);
	    return new ResponseEntity<>(grad , HttpStatus.ACCEPTED );
	}
	
LoggerConfig.logger.info("invalid token");
 Error error = new Error();
 error.message = "error";
 return new ResponseEntity<>(error, HttpStatus.ACCEPTED);
}

@GetMapping("/trends")
public ResponseEntity<?> getTrend(@RequestParam String year , @RequestParam String type, @RequestHeader("token") String token ) throws GeneralSecurityException, IOException
{
	
	if(validator.isValid(token))
	{ 
		LoggerConfig.logger.info("valid token");
		List<Trend> arl=gradService.findTrend(year, type);
		 return new ResponseEntity<>(arl, HttpStatus.OK);
		
	}
LoggerConfig.logger.info("invalid token");
 Error error = new Error();
 error.message = "error";
 return new ResponseEntity<>(error, HttpStatus.ACCEPTED);
}

}




