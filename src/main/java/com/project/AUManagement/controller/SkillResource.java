package com.project.AUManagement.controller;


import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.project.AUManagement.config.LoggerConfig;
import com.project.AUManagement.domain.Error;
import com.project.AUManagement.domain.Skill;
import com.project.AUManagement.repository.SkillRepository;
import com.project.AUManagement.service.SkillService;
import com.project.AUManagement.service.Validator;


@CrossOrigin(origins="*",allowedHeaders = "*")
@RestController
public class SkillResource {
	
	@Autowired
	private SkillService skillService;
	@Autowired
	private Validator validator;
	
	
	@GetMapping("/skills")
	public ResponseEntity<?> getAll(@RequestHeader("token") String token ) throws GeneralSecurityException, IOException
	{
		//MyLogger.setup()
		if(validator.isValid(token))
		{
		LoggerConfig.logger.info("valid token");
		List<Skill> arl;
		arl= skillService.findAll();
		return new ResponseEntity<>(arl, HttpStatus.ACCEPTED);
		}
	LoggerConfig.logger.info("invalid token");
    Error error = new Error();
    error.message = "error";
	return new ResponseEntity<>(error, HttpStatus.ACCEPTED);
	}
	
	
	@GetMapping("/skills/{id}")
	public ResponseEntity<?> getskill(@PathVariable long id,@RequestHeader("token") String token ) throws GeneralSecurityException, IOException
	{
		if(validator.isValid(token))
		{ 	LoggerConfig.logger.info("valid token");
			Skill skill=skillService.findById(id);
			return new ResponseEntity<>(skill, HttpStatus.ACCEPTED);
		}
	LoggerConfig.logger.info("invalid token");
    Error error = new Error();
    error.message = "error";
	return new ResponseEntity<>(error, HttpStatus.ACCEPTED);
	}
	
	
	@PostMapping("/skills")
	public ResponseEntity<?> addSkill(@RequestBody Skill s1,@RequestHeader("token") String token ) throws GeneralSecurityException, IOException
	{
		if(validator.isValid(token))
		{ 	LoggerConfig.logger.info("valid token");
			Skill skill=skillService.save(s1);
			return new ResponseEntity<>( skill , HttpStatus.CREATED);
		}
	LoggerConfig.logger.info("invalid token");
    Error error = new Error();
    error.message = "error";
	return new ResponseEntity<>(error, HttpStatus.ACCEPTED);
	}
	

}
