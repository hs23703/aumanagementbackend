package com.project.AUManagement.repository;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.project.AUManagement.config.LoggerConfig;
import com.project.AUManagement.domain.Skill;


@Repository
public class SkillRepository {
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public Skill save(Skill skill)
	{
		String insertQuery;
		insertQuery="insert into skill(" +
				"name, created_by, created_on)" +
				"Values( ?,?,? )";
		jdbcTemplate.update(insertQuery, skill.getName() , skill.getCreatedBy() , skill.getCreatedOn());
		LoggerConfig.logger.info("New SKill Added");
		 return skill;
	}
	
	public List<Skill> findAll()
	{
		String query = "SELECT * FROM skill";
		List<Skill> skills = jdbcTemplate.query(
		  query, new SkillRowMapper());
		
		return skills;
	}
	public Skill findById(long id)
	{
		String query = "SELECT * FROM skill where id ="+id;
		Skill s1=jdbcTemplate.queryForObject(query,new SkillRowMapper());
		return s1;
	}
	
	public void deleteTestSkill()
	{
		String deleteQuery="delete from skill where name=\"testskill\";";
		jdbcTemplate.update(deleteQuery);
		
	}

}
//skill.getName() , skill.getCreatedBy() , skill.getCreatedOn()
