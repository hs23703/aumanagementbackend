package com.project.AUManagement.repository;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.project.AUManagement.domain.Grad;

public class GradRowMapper implements RowMapper <Grad> {

	@Override
	public Grad mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		Grad grad=new Grad();
		
		grad.setId(rs.getLong("id"));
		grad.setBranch(rs.getString("branch"));
		grad.setContact(rs.getString("contact"));
		grad.setCreatedBy(rs.getString("created_by"));
		grad.setCreatedOn(rs.getDate("created_on").toString());
		grad.setDegree(rs.getString("degree"));
		grad.setEmail(rs.getString("email"));
		grad.setFirstName(rs.getString("first_name"));
		grad.setGender(rs.getString("gender"));
		grad.setInstitute(rs.getString("institute"));
		grad.setJoiningDate(rs.getDate("joining_date").toString());
		grad.setJoiningLocation(rs.getString("joining_location"));
		grad.setLastName(rs.getString("last_name"));
		grad.setMiddleName(rs.getString("middle_name"));
		grad.setParentId(rs.getLong("parent_id"));
		
		return grad;
	}

}
