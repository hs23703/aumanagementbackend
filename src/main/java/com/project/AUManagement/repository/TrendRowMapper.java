package com.project.AUManagement.repository;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.project.AUManagement.domain.Trend;

public class TrendRowMapper implements RowMapper {

	@Override
	public Trend mapRow(ResultSet rs, int rowNum) throws SQLException {
		Trend trend=new Trend();
		trend.setName(rs.getString("name"));
		trend.setCount(rs.getString("count"));
		return trend;
	}

}
