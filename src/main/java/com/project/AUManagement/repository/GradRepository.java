package com.project.AUManagement.repository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.project.AUManagement.config.LoggerConfig;
import com.project.AUManagement.domain.Grad;
import com.project.AUManagement.domain.Skill;
import com.project.AUManagement.domain.Trend;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;


@Repository
public class GradRepository  {

	@Autowired
	JdbcTemplate jdbcTemplate;
	
	
	public Grad save(Grad grad)
	{	
		
		//analyze th schema information
		String analyzeQuery="ANALYZE TABLE grad";
		jdbcTemplate.update(analyzeQuery);
		//get auto-increament value
		String autoIncQuery=" SELECT AUTO_INCREMENT\r\n" + 
				"FROM information_schema.TABLES\r\n" + 
				"WHERE TABLE_SCHEMA = \"db_aumanagement\"\r\n" + 
				"AND TABLE_NAME = \"grad\"";
		long id=jdbcTemplate.queryForObject(autoIncQuery, Long.class);
		
		//save query
		String insertQuery;
		insertQuery="insert into grad (" +
				"branch ,contact, created_by, created_on, degree,"+
				"email, first_name, gender, institute, joining_date," +
				"joining_location, last_name, middle_name, parent_id, status " +
				")" +
				"VALUES ( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )";
		jdbcTemplate.update(insertQuery, grad.getBranch() , grad.getContact() , grad.getCreatedBy(), LocalDateTime.now(), grad.getDegree(),
				grad.getEmail(), grad.getFirstName() , grad.getGender() , grad.getInstitute(), grad.getJoiningDate() ,
				grad.getJoiningLocation(), grad.getLastName(), grad.getMiddleName(),id,1 );
		
		
		//update in grad_skill table
		String skillInsertQuery="insert into grad_skill ( grad_id , skill_id ) values ( ?,? )";
		for(Skill skill:grad.getSkills())
		{
			jdbcTemplate.update(skillInsertQuery,id,skill.getId());
		}
		LoggerConfig.logger.info("Grad Created");
		grad=findById(id);
		return grad;
		
	}
	
	
	public List<Grad> findAll(){
	String selectQuery="select * from grad where status = "+ 1 +" order by parent_id asc ";
	List<Grad> grads;
	grads=jdbcTemplate.query(selectQuery, new GradRowMapper() );
     
		return grads;
	}

	
	public Grad findById(long id) {
		
		String selectQuery="select * from grad where id="+id +" ";
		Grad grad= jdbcTemplate.queryForObject(selectQuery, new GradRowMapper());
		
		//get the skills too
		String skillSelectQuery="select * from skill" +
				" where id in(select skill_id from grad_skill where grad_id=" + grad.getId() +")" ;
		List<Skill> skills=jdbcTemplate.query(skillSelectQuery,new SkillRowMapper() );
		grad.setSkills(skills);
		
		return grad;
		
		
	}
	

	public Grad deleteById(long id) {

		//delete the skills first
		Grad grad=findById(id);
		
		//setting the status of  grad to 0 marked as deleted
		String updateQuery="update grad set status= "+ 0 +"  where id= "+ id + " ";
		jdbcTemplate.update(updateQuery);
		
		LoggerConfig.logger.info("Grad Deleted");
		
		return grad;
		
	}
	
	public Grad updateById(Grad grad) {
		
		//setting the status of existing grad to 0
		String updateQuery="update grad set status= "+ 0 +"  where id= "+ grad.getId() + " ";
		jdbcTemplate.update(updateQuery);
		
		//enter the new grad
		String insertQuery="insert into grad (" +
				"branch ,contact, created_by, created_on, degree,"+
				"email, first_name, gender, institute, joining_date," +
				"joining_location, last_name, middle_name, parent_id, status " +
				")" +
				"VALUES ( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )";
		
		jdbcTemplate.update(insertQuery, grad.getBranch() , grad.getContact() , grad.getCreatedBy(), LocalDateTime.now(), grad.getDegree(),
				grad.getEmail(), grad.getFirstName() , grad.getGender() , grad.getInstitute(), grad.getJoiningDate() ,
				grad.getJoiningLocation(), grad.getLastName(), grad.getMiddleName(), grad.getParentId(),1 );	
		
		
		//update int schema info
		String analyzeQuery="ANALYZE TABLE grad";
		jdbcTemplate.update(analyzeQuery);
		
		//get the autoincrement value
		String autoIncQuery="SELECT AUTO_INCREMENT\r\n" + 
				"FROM information_schema.TABLES\r\n" + 
				"WHERE TABLE_SCHEMA = \"db_aumanagement\"\r\n" + 
				"AND TABLE_NAME = \"grad\"";		
		long id=jdbcTemplate.queryForObject(autoIncQuery, Long.class);
		id--;
		
		//update in grad_skill table
		String skillInsertQuery="insert into grad_skill ( grad_id , skill_id ) values ( ?,? )";
		for(Skill skill:grad.getSkills())
		{
			jdbcTemplate.update(skillInsertQuery,id,skill.getId());
		}
		
		LoggerConfig.logger.info("Grad updaated");
		
	 return findById(id);
		
		}


	public List<Trend> findTrend(String year,String type) {
		String selectQuery;
        switch(type)
        {
        case  "location" :
        {
          selectQuery="select joining_location as name , count(*) as count from grad\r\n" + 
           "where extract(year from joining_date)=" +year+ "\r\n" + 
           "group by joining_location;";
          return jdbcTemplate.query(selectQuery, new TrendRowMapper() );
        }
        case "skill" :
        {
    		selectQuery="select name ,count \r\n" + 
    				"from skill , \r\n" + 
    				"(select skill_id, count(grad_id) as count from grad_skill\r\n" + 
    				"where grad_id in (select id from grad where status =1 and extract(year from joining_date)= "+ year + ") \r\n" + 
    				"group by skill_id) as trend\r\n" + 
    				"where id=trend.skill_id;";
    		 return jdbcTemplate.query(selectQuery, new TrendRowMapper() );
    			
        }
       
        }
        
		return null;
		
	}
	public Grad deleteForTesting(Grad grad)
	{
		String deleteQueryGradSkill="delete from grad_skill where grad_id="+grad.getId();
		jdbcTemplate.update(deleteQueryGradSkill);
		
		String deleteQuery="delete from grad where id="+grad.getId();
		jdbcTemplate.update(deleteQuery);
		
		return grad;
	}
	
	
	}
