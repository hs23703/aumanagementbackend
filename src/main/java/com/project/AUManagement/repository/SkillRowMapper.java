package com.project.AUManagement.repository;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.project.AUManagement.domain.Skill;

public class SkillRowMapper implements RowMapper<Skill> {
	
    @Override
    public Skill mapRow(ResultSet rs, int rowNum) throws SQLException {
        Skill skill = new Skill();
 
        skill.setId(rs.getLong("id"));
        skill.setName(rs.getString("name"));
        skill.setCreatedOn(rs.getString("created_on"));
        skill.setCreatedBy(rs.getString("created_by"));
        return skill;
    }
}