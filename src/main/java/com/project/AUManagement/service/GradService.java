package com.project.AUManagement.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.project.AUManagement.domain.Grad;
import com.project.AUManagement.domain.Trend;
import com.project.AUManagement.repository.GradRepository;

@Service
public class GradService {
	@Autowired
	GradRepository gradRepository;

	public List<Grad> findAll() {

		return gradRepository.findAll();
	}

	public Grad findById(long id) {
		// TODO Auto-generated method stub
		return gradRepository.findById(id);
	}

	public Grad save(Grad g1) {
		// TODO Auto-generated method stub
		return gradRepository.save(g1);
	}

	
	public Grad updateById(Grad g1) {
		// TODO Auto-generated method stub
		return gradRepository.updateById(g1);
	}

	public Grad deleteById(long id) {
		// TODO Auto-generated method stub
		return gradRepository.deleteById(id);
	}

	public List<Trend> findTrend(String year,String type) {
		// TODO Auto-generated method stub
		return gradRepository.findTrend(year,type);
	}	
}
