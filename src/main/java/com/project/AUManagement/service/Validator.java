package com.project.AUManagement.service;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.project.AUManagement.domain.User;

@Component
public class Validator {

public boolean isValid(String token) throws GeneralSecurityException, IOException
{
//	System.out.println(token);
	//add user in the database
	//if exits update the token
	HttpTransport transport=GoogleNetHttpTransport.newTrustedTransport();
	JsonFactory jsonFactory = new JacksonFactory();

	GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(transport, jsonFactory)
		    .setAudience(Collections.singletonList("286959256087-6afke0oct4p0vuleh2uabjl22baev2rj.apps.googleusercontent.com"))
		    .build();
		GoogleIdToken idToken = verifier.verify(token);
		if (idToken != null) {
		  Payload payload = idToken.getPayload();
		  // Print user identifier
		  String userId = payload.getSubject();
//		  System.out.println("User ID: " + userId);
		  
		  // Get profile information from payload
		  String email = payload.getEmail();
		  boolean emailVerified = Boolean.valueOf(payload.getEmailVerified());
	     String name = (String) payload.get("name");
//		  String pictureUrl = (String) payload.get("picture");
//		  String locale = (String) payload.get("locale");
//		  String familyName = (String) payload.get("family_name");
//		  String givenName = (String) payload.get("given_name");
//		  System.out.println(name);
		  return emailVerified;

		} else {
		  System.out.println("Invalid ID token.");
		  return false;
		}
	
	
}


}
	

