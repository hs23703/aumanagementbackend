package com.project.AUManagement.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.AUManagement.domain.Skill;
import com.project.AUManagement.repository.SkillRepository;

@Service
public class SkillService {

	@Autowired
	private SkillRepository skillRepository;

	public List<Skill> findAll() {
		// TODO Auto-generated method stub
		return skillRepository.findAll();
	}

	public Skill findById(long id) {
		// TODO Auto-generated method stub
		return skillRepository.findById(id);
	}

	public Skill save(Skill s1) {
		// TODO Auto-generated method stub
		return skillRepository.save(s1);
		
	}
}
