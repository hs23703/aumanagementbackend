CREATE TABLE if not exists db_aumanagement.grad (
  `id` bigint NOT NULL auto_increment,
  `branch` varchar(255) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_on` DateTime DEFAULT NULL,
  `degree` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `institute` varchar(255) DEFAULT NULL,
  `joining_date` Date DEFAULT NULL,
  `joining_location` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
   parent_id bigint default 0,
   status bool default 0,
  PRIMARY KEY (`id`)
);
CREATE TABLE if not exists db_aumanagement.skill (
  `id` bigint NOT NULL auto_increment,
  `created_by` varchar(255) DEFAULT NULL,
  `created_on`  DateTime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`)
);

 CREATE TABLE if not exists db_aumanagement.grad_skill (
  `grad_id` bigint NOT NULL,
  `skill_id` bigint NOT NULL,
  PRIMARY KEY (grad_id,skill_id),
  CONSTRAINT fk1 FOREIGN KEY (`skill_id`) REFERENCES `skill` (`id`),
  CONSTRAINT fk2 FOREIGN KEY (`grad_id`) REFERENCES `grad` (`id`)
  
) ;

Create Table if not exists db_aumanagement.user(
email varchar(255) not null,
name varchar(255) default null,
primary key(email)
);





	